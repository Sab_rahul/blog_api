from django.contrib.auth import authenticate
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView,RetrieveUpdateDestroyAPIView,CreateAPIView
from rest_framework.authentication import BasicAuthentication
from .permissions import getpermission
from .models import User,Blog
from .serializers import UserLoginSerializer, UserRegisterSerializer,BlogSerializer
# Create your views here.

# from rest_framework_simplejwt.tokens import RefreshToken

# def get_tokens_for_user(user):
#     refresh = RefreshToken.for_user(user)

#     return {
#         'refresh': str(refresh),
#         'access': str(refresh.access_token),
#     }

class UserRegisterView(APIView):
    def post(self,request,format=None):
        serializer=UserRegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({'msg':'User saved Successfully'},status=status.HTTP_201_CREATED)

class UserLoginView(APIView):
    def post(self,request,format=None):
        serializer=UserLoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        email=serializer.data.get('email')
        password=serializer.data.get('password')
        user=authenticate(email=email,password=password)
        if user is not None:
            # token=get_tokens_for_user(user)
            return Response({'msg':'User login Successfully'},status=status.HTTP_200_OK)
        else:
            return Response({'error':'User does not exist'},status=status.HTTP_400_BAD_REQUEST)


class BlogListCreateView(ListCreateAPIView):
    authentication_classes=[BasicAuthentication]
    permission_classes=(getpermission,)
    queryset=Blog.objects.all()
    serializer_class=BlogSerializer

    def create(self,request,*args,**kwargs):
        current_user=User.objects.get(id=request.user.id)
        if request.data['creater']!=current_user.id:
            return Response({'error':'creator must be requesting user'},status=status.HTTP_400_BAD_REQUEST)
        return super().create(request,*args, **kwargs)


class BlogRetriveUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    authentication_classes=[BasicAuthentication]
    permission_classes=(getpermission,)
    queryset=Blog.objects.all()
    serializer_class=BlogSerializer

    def put(self, request, *args, **kwargs):
        current_user=User.objects.get(id=request.user.id)
        if request.data['creater']!=current_user.id:
            return Response({'error':'creator must be requesting user'},status=status.HTTP_400_BAD_REQUEST)
        return super().put(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        current_user=User.objects.get(id=request.user.id)
        if request.data['creater']!=current_user.id:
            return Response({'error':'creator must be requesting user'},status=status.HTTP_400_BAD_REQUEST)
        return super().patch(request, *args, **kwargs)
  
    

