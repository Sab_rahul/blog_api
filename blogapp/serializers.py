from rest_framework import serializers
from .models import User,Blog

class UserRegisterSerializer(serializers.ModelSerializer):
    # password=serializers.CharField(style={'input_type':'password'})
    password2=serializers.CharField(style={'input_type':'password'},write_only=True)
    class Meta:
        model=User
        fields=['id','name','email','password','password2']

    def validate(self,attrs):
        password=attrs.get('password')
        password2=attrs.get('password2')
        if password!=password2:
            raise serializers.ValidationError('passwords do not match')
        return attrs

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)

class UserLoginSerializer(serializers.ModelSerializer):
    email=serializers.EmailField(max_length=55)
    class Meta:
        model=User
        fields=['email','password']


class BlogSerializer(serializers.ModelSerializer):
    class Meta:
        model=Blog
        fields=['id','creater','title','blog']

    