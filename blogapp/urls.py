from atexit import register
from posixpath import basename
from django.urls import path,include
# from rest_framework.routers import DefaultRouter

from .views import BlogListCreateView, BlogRetriveUpdateDeleteView, UserLoginView,UserRegisterView

# router=DefaultRouter()


# router.register('register/',UserRegisterView,basename='register')



urlpatterns = [
    path('api/user/register/',UserRegisterView.as_view(),name='register'),
    path('api/user/login/',UserLoginView.as_view(),name='login'),
    path('api/user/blog/get/',BlogListCreateView.as_view(),name='blog_get'),
    # path('api/user/blog/create/',BlogCreateView.as_view(),name='blog_create'),
    path('api/user/blog/get/<int:pk>/',BlogRetriveUpdateDeleteView.as_view(),name='blog_reteive_update_delete'),

    # path('api/user/',include(router.urls))
]
