from django.contrib import admin
from .models import User,Blog
# Register your models here.
@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display= ['id','name','email','is_staff','is_superuser']

@admin.register(Blog)
class BlogAdmin(admin.ModelAdmin):
    list_display= ['id','creater','title','blog']